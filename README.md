This project is part of the [devops](github.com/KTH/devops-course/) course at the KTH. It aims to be an open project with three focuses:

The core of the project is a javascript-based digital version of the amazing board game Flamme Rouge by Asger Harding. Additionally we set a further focus on having a nice infrastructure to deploy this webapp and support further extensions in any direction. Furthermore, as we are copying a board game, we wrote a short thesis on Intellectual Property, as we were totally unaware of what we are allowed to do and what we are should not copy or reuse.

# Lanterne Rouge - the game
Lanterne Rouge is the implementation of single-player version of Flamme Rouge, the board game. It is created as a Java-Script based single-page application and uses the [p5js](p5js.org) library mainly for the graphics. The game mechanics are quite complex which resulted in several and interdependent funtions.

The application handles drawing, user interaction and automatic game board building and management of mechanics. Additionally the game provides AI-controlled opponents based on the rules of the board game. The user can choose their preferred color and the amount of games he/she wants to play. The movement mechanics vary based on the type of the board. Focus was also on extension of the game, with an easy board creation class. The background functions had to be made asychronous, in order for us to be able control the timing of events.

The whole application can be run by running the index.html file in /src folder and can be run in Chromium-based browsers, in addition to Safari.

## Running the game
The game is deployed automatically and can be found [here](https://lanterne-rouge.stephan.host)

# Infrastructure
To bring our webapp online we knowingly chose quite a cumbersome and overengineered way, which on the other hand supports the implementation of further features very well. We wanted to host it on kubernetes and have a CI/CD pipeline on gitlab which automatically deploys changes on the master branch. With the current setup it would be easy to extend it for a staging environment for example. This could help a lot in possible further extension of the functionalities.

Pushing to the repo triggers the Gitlab CI/CD pipeline, which builds a small docker exposing a port with a nginx serving the website. Futhermore we use Helm for package management of the k8s manifests and to automagically deploy the newly build image in the pipeline, provided the change was on the master. Helm will ensure that the pod running the old image will be killed so that the new version is reachable via the url: https://lanterne-rouge.stephan.host

A more precise description of how we implemented the automatic deployment with Gitlab CI/CD with a focus on Helm can be found here: https://github.com/stephanhorsthemke/50-days-CNCF

Additionally we use Let's encrypt to secure the website which we automated with the certmanager on kubernetes. Also we use nginx-ingress for the direction to the service. 

In other words, our infrastructure is all set to be extended in any direction and scale nicely :)


## Known bugs
- Due to P5js limitations, resizing the screen while ingame, will lead to the board not being updated resulting in sut edges.
- Playtesting has revealed that the game presentation is wrong on High DPI screens as neither member of the group has such screen to test the game on.

# Intellectual Property
For the Intellectual Property topic we decided to create an actual paper which you view in the [Intellectual Property directory](IntellectualProperty/IntellectualProperty_tex.pdf).