FROM nginx

ADD src /usr/share/nginx/html

CMD nginx -g "daemon off;"